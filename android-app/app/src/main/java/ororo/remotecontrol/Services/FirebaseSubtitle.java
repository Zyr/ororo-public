package ororo.remotecontrol.Services;

import com.google.firebase.database.PropertyName;

public class FirebaseSubtitle {

    @PropertyName("subtitleName")
    public String SubtitleName;

    @PropertyName("subtitleUrl")
    public String SubtitleUrl;
}

