package ororo.remotecontrol.Services;

import android.content.Context;
import android.support.annotation.NonNull;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Date;

import okhttp3.ResponseBody;
import ororo.remotecontrol.Api.Ororo.OroroServiceApi;
import ororo.remotecontrol.Subtitle;
import ororo.remotecontrol.SubtitleLine;
import ororo.remotecontrol.Utils.DateUtils;
import retrofit2.Call;
import retrofit2.Callback;

import static ororo.remotecontrol.Utils.TextUtils.isInteger;
import static ororo.remotecontrol.Utils.TextUtils.parseDate;

public class SubtitleService {
    private OroroServiceApi mOroroServiceApi;
    private ArrayList<SubtitleLine> contextualTranslateSubtitles;

    public SubtitleService() {
        mOroroServiceApi = OroroServiceApi.retrofit.create(OroroServiceApi.class);
    }

    public void downloadSubtitles(
            final Context context,
            final Subtitle originalSubtitles,
            final Subtitle translatedSubtitles,
            final OnSubtitlesDownloadedListener onSubtitlesDownloadedListener) {

        downloadSubtitle(context, originalSubtitles, new OnSubtitlesDownloadedListener(){
            @Override
            public void onDownload(final ArrayList<SubtitleLine> originalSubtitleLines) {
                downloadSubtitle(context, translatedSubtitles, new OnSubtitlesDownloadedListener(){
                    @Override
                    public void onDownload(ArrayList<SubtitleLine> translatedSubtitleLines) {
                        contextualTranslateSubtitles = translatedSubtitleLines;
                        onSubtitlesDownloadedListener.onDownload(originalSubtitleLines);
                    }
                });
            }
        });
    }

    public String getContextualTranslate(Date date) {
        ArrayList<SubtitleLine> subtitles = contextualTranslateSubtitles;
        if (subtitles != null) {
            for (SubtitleLine subtitle : subtitles) {
                if (DateUtils.isInPeriod(subtitle.From, subtitle.To, date))
                    return subtitle.Line;
                if (subtitle.From.after(date))
                    return null;
            }
        }
        return null;
    }

    private void downloadSubtitle(final Context context, final Subtitle subtitle, final OnSubtitlesDownloadedListener onSubtitlesDownloadedListener) {
        if (subtitle.Name == null)
            onSubtitlesDownloadedListener.onDownload(new ArrayList<SubtitleLine>());
        mOroroServiceApi.downloadSubtitles(subtitle.Url).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull retrofit2.Response<ResponseBody> response) {
                ArrayList<SubtitleLine> parsedSubtitles = Parse(context, response.body().byteStream());
                onSubtitlesDownloadedListener.onDownload(parsedSubtitles);
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                Toast.makeText(context, "Can't download subtitles: " + subtitle.Name + " " + t.getMessage(), Toast.LENGTH_SHORT).show();
                onSubtitlesDownloadedListener.onDownload(new ArrayList<SubtitleLine>());
            }
        });
    }

    private static ArrayList<SubtitleLine> Parse(Context context, InputStream inputStream) {
        ArrayList<SubtitleLine> subtitleLines = new ArrayList<>();
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
            String mLine;
            while ((mLine = reader.readLine()) != null) {
                String lineNumber = mLine;
                if (isInteger(lineNumber)) {
                    String timeLine = reader.readLine();
                    String[] splits = timeLine.split(" --> ");
                    if (splits.length == 2) {
                        Date from = parseDate(splits[0]);
                        Date to = parseDate(splits[1]);
                        if (from != null && to != null) {
                            String text = "";
                            while ((mLine = reader.readLine()) != null && !"".equals(mLine))
                                text += mLine + " ";
                            subtitleLines.add(new SubtitleLine(text, from, to));
                        }
                    }
                }
            }
            return subtitleLines;
        } catch (Exception e) {
            Toast.makeText(context, "Can't parse subtitles: " + e.getMessage(), Toast.LENGTH_SHORT).show();
            return subtitleLines;
        }
    }
}
