package ororo.remotecontrol.Services;

import ororo.remotecontrol.Api.Leo.TranslateServiceApi;
import ororo.remotecontrol.Api.Leo.Translation;
import retrofit2.Call;

public class TranslateService {
    private TranslateServiceApi mTranslateServiceApi;

    public TranslateService() {
        mTranslateServiceApi = TranslateServiceApi.retrofit.create(TranslateServiceApi.class);
    }

    public Call<Translation> getTranslate(String word) {
        return mTranslateServiceApi.getTranslate(word);
    }
}
