package ororo.remotecontrol.Services;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Date;

import ororo.remotecontrol.Subtitle;

import static ororo.remotecontrol.Utils.TextUtils.parseDate;

public class FirebaseService {
    private static DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();

    public void stopOrPlayVideo() {
        mDatabase.child(getUid()).child("playerStatus").setValue(Math.random());
    }

    public void addOnSubtitlesChangedListener(final String originalSubtitlesName, final String translatedSubtitlesName, final OnSubtitlesChangeListener onSubtitlesChangeListener) {
        mDatabase.child(getUid()).child("currentVideo").child("subtitles").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot subtitlesSnapshot) {
                Subtitle originalSubtitles = null;
                Subtitle translatedSubtitles = null;
                for (DataSnapshot subtitleSnapshot : subtitlesSnapshot.getChildren()) {
                    final FirebaseSubtitle subtitle = subtitleSnapshot.getValue(FirebaseSubtitle.class);
                    if (subtitle != null) {
                        if (originalSubtitlesName.equalsIgnoreCase(subtitle.SubtitleName))
                            originalSubtitles = new Subtitle(subtitle.SubtitleName, subtitle.SubtitleUrl);
                        if (translatedSubtitlesName.equalsIgnoreCase(subtitle.SubtitleName))
                            translatedSubtitles = new Subtitle(subtitle.SubtitleName, subtitle.SubtitleUrl);
                    }
                }
                onSubtitlesChangeListener.onChanged(originalSubtitles, translatedSubtitles);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }


    public void onCurrentVideoTimeChanged(final OnCurrentVideoTimeChangeListener onCurrentVideoTimeChangeListener){
        mDatabase.child(getUid()).child("currentVideo").child("time").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Object value = dataSnapshot.getValue();
                if (value != null) {
                    Date date = parseDate(value.toString());
                    if (date != null) {
                        onCurrentVideoTimeChangeListener.onChanged(date);
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    public void setCurrentVideoTime(Date time) {
        mDatabase.child(getUid()).child("currentVideo").child("setTime").setValue(time.getTime());
    }

    private String getUid() {
        return FirebaseAuth.getInstance().getCurrentUser().getUid();
    }
}
