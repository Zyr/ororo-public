package ororo.remotecontrol;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Point;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.Display;
import android.view.View;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;

import java.util.ArrayList;
import java.util.Date;

import ororo.remotecontrol.Api.Leo.Translation;
import ororo.remotecontrol.Controls.OnWordClickListener;
import ororo.remotecontrol.Controls.SubtitleLineView;
import ororo.remotecontrol.Controls.TranslateDialog;
import ororo.remotecontrol.Services.FirebaseService;
import ororo.remotecontrol.Services.OnCurrentVideoTimeChangeListener;
import ororo.remotecontrol.Services.OnSubtitlesChangeListener;
import ororo.remotecontrol.Services.OnSubtitlesDownloadedListener;
import ororo.remotecontrol.Services.SubtitleService;
import ororo.remotecontrol.Services.TranslateService;
import ororo.remotecontrol.Utils.DateUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static ororo.remotecontrol.Utils.TextUtils.getClearWord;

public class MainActivity extends BaseActivity {

    private LinearLayout mSubtitlesLinearLayout;
    private ScrollView mScrollView;
    private View mCurrentSubtitleLineView;
    private CheckBox mStickToActiveSubtitle;
    private Point displaySize;
    private TranslateDialog mDialog;

    private TranslateService mTranslateService;
    private FirebaseService mFirebaseService;
    private SubtitleService mSubtitleService;
    private boolean mVideoStopped;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mScrollView = findViewById(R.id.scrollView);
        mSubtitlesLinearLayout = findViewById(R.id.subtitles);
        mStickToActiveSubtitle = findViewById(R.id.stickToActiveSubtitle);
        mCurrentSubtitleLineView = null;
        Display display = getWindowManager().getDefaultDisplay();
        displaySize = new Point();
        display.getSize(displaySize);

        mTranslateService = new TranslateService();
        mSubtitleService = new SubtitleService();
        mFirebaseService = new FirebaseService();

        mDialog = new TranslateDialog(this, mSubtitleService);
        mDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                if (!mVideoStopped) {
                    mFirebaseService.stopOrPlayVideo();
                }
                hideProgressDialog();
            }
        });

        mFirebaseService.addOnSubtitlesChangedListener("en", "ru", createOnSubtitlesChangedListener());
        mFirebaseService.onCurrentVideoTimeChanged(createOnCurrentVideoTimeChangedListener());
    }

    private OnSubtitlesChangeListener createOnSubtitlesChangedListener() {
        return new OnSubtitlesChangeListener() {
            public void onChanged(Subtitle originalSubtitles, Subtitle translatedSubtitles) {
                if (originalSubtitles != null) {
                    mSubtitleService.downloadSubtitles(MainActivity.this, originalSubtitles, translatedSubtitles, new OnSubtitlesDownloadedListener() {
                        @Override
                        public void onDownload(ArrayList<SubtitleLine> subtitles) {
                            UpdateSubtitlesView(subtitles);
                        }
                    });
                }
            }
        };
    }

    private OnCurrentVideoTimeChangeListener createOnCurrentVideoTimeChangedListener() {
        return new OnCurrentVideoTimeChangeListener() {
            @Override
            public void onChanged(Date date) {
                View view = findSubtitleLineByDate(date);
                if (view != null) {
                    if (mCurrentSubtitleLineView != null) {
                        mCurrentSubtitleLineView.setBackgroundColor(Color.WHITE);
                    }
                    mCurrentSubtitleLineView = view;
                    if (mStickToActiveSubtitle.isChecked()) {
                        goToActiveSubtitle();
                    }
                    view.setBackgroundColor(Color.YELLOW);
                }
            }
        };
    }

    private void UpdateSubtitlesView(ArrayList<SubtitleLine> subtitleLines) {
        mSubtitlesLinearLayout.removeAllViews();
        for (final SubtitleLine subtitleLine : subtitleLines) {
            SubtitleLineView subtitleLineView = new SubtitleLineView(this, subtitleLine, new OnWordClickListener() {
                @Override
                public void onWordClick(final String word) {
                    final String clearWord = getClearWord(word);
                    if (!mVideoStopped) {
                        mFirebaseService.stopOrPlayVideo();
                    }
                    showProgressDialog();
                    mTranslateService.getTranslate(clearWord).enqueue(new Callback<Translation>() {
                        @Override
                        public void onResponse(@NonNull Call<Translation> call, @NonNull Response<Translation> response) {
                            hideProgressDialog();
                            Translation translation = response.body();
                            mDialog.updateTranslation(subtitleLine, word, translation);
                            mDialog.show();
                        }

                        @Override
                        public void onFailure(@NonNull Call<Translation> call, @NonNull Throwable t) {
                            hideProgressDialog();
                            Toast.makeText(MainActivity.this, "Get translation failed: " + t.getMessage(), Toast.LENGTH_SHORT).show();
                            if (!mVideoStopped) {
                                mFirebaseService.stopOrPlayVideo();
                            }
                        }
                    });
                }

                @Override
                public void onSentenceLongClick(SubtitleLine subtitleLine) {
                    mFirebaseService.setCurrentVideoTime(subtitleLine.From);
                    if (mVideoStopped) {
                        mFirebaseService.stopOrPlayVideo();
                        mVideoStopped = false;
                    }
                }
            });
            subtitleLineView.setTag(subtitleLine);
            mSubtitlesLinearLayout.addView(subtitleLineView);
        }
    }

    @Nullable
    private View findSubtitleLineByDate(Date date) {
        int count = mSubtitlesLinearLayout.getChildCount();
        for (int i = 0; i < count; i++) {
            View view = mSubtitlesLinearLayout.getChildAt(i);
            Object tag = view.getTag();
            SubtitleLine subtitleLine = (tag instanceof SubtitleLine ? (SubtitleLine) tag : null);
            if (subtitleLine != null) {
                if (DateUtils.isInPeriod(subtitleLine.From, subtitleLine.To, date))
                    return view;
                if (subtitleLine.From.after(date))
                    return null;
            }
        }
        return null;
    }

    public void onPlayerStatusClicked(View view) {
        mVideoStopped = !mVideoStopped;
        mFirebaseService.stopOrPlayVideo();
    }

    private void goToActiveSubtitle() {
        View currentSubtitleLineView = mCurrentSubtitleLineView;
        if (currentSubtitleLineView != null) {
            mScrollView.scrollTo(0, (int) currentSubtitleLineView.getY() - (int) (displaySize.y * 0.5));
        }
    }

    public void onSignOutClicked(View view) {
        FirebaseAuth.getInstance().signOut();
        startActivity(new Intent(this, SignInActivity.class));
        finish();
    }
}
