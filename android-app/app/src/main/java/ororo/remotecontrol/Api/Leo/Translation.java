package ororo.remotecontrol.Api.Leo;

import com.google.gson.annotations.SerializedName;

public class Translation {
    @SerializedName("translate")
    public Translate[] Translates;

    @SerializedName("transcription")
    public String Transcription;
}
