package ororo.remotecontrol.Api.Ororo;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.http.GET;
import retrofit2.http.Url;

public interface OroroServiceApi {
    @GET
    Call<ResponseBody> downloadSubtitles(@Url String subtitlesUrl);

    Retrofit retrofit = new Retrofit.Builder().baseUrl("https://fakeUrl").build();
}
