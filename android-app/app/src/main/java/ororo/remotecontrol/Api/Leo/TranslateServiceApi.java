package ororo.remotecontrol.Api.Leo;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface TranslateServiceApi {
    @GET("gettranslates")
    Call<Translation> getTranslate(@Query("word") String word);

    Retrofit retrofit = new Retrofit.Builder()
            .baseUrl("http://api.lingualeo.com/")
            .addConverterFactory(GsonConverterFactory.create())
            .build();
}
