package ororo.remotecontrol.Api.Leo;

import com.google.gson.annotations.SerializedName;

public class Translate {
    @SerializedName("id")
    public int Id;

    @SerializedName("value")
    public String Value;

    @SerializedName("votes")
    public int Votes;

    @SerializedName("pic_url")
    public String PictureUrl;
}
