package ororo.remotecontrol;

public class Subtitle {
    public String Name;
    public String Url;

    public Subtitle(String subtitleName, String subtitleUrl) {
        Name = subtitleName;
        Url = subtitleUrl;
    }
}

