package ororo.remotecontrol;

import java.util.Date;

import ororo.remotecontrol.Utils.TextUtils;

public class SubtitleLine {
    public String Line;
    public String[] Words;
    public Date From;
    public Date To;

    public SubtitleLine(String line, Date from, Date to) {
        Line = TextUtils.clearSentence(line);
        From = from;
        To = to;
        Words = Line.split("\\s+");
    }

    @Override
    public String toString() {
        return "SubtitleLine{" +
                "Line='" + Line + '\'' +
                ", From=" + From +
                ", To=" + To +
                '}';
    }
}
