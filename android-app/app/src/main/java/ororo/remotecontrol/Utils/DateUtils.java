package ororo.remotecontrol.Utils;

import java.util.Date;

public class DateUtils {
    public static boolean isInPeriod(Date from, Date to, Date date){
        if (from == null || to == null || date == null)
            return false;
        if ((from.before(date) || from.equals(date)) && (date.before(to) || date.equals(to))) {
            return true;
        }
        return false;
    }
}
