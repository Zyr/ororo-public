package ororo.remotecontrol.Utils;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class TextUtils {

    private static SimpleDateFormat dateFormat1 = new SimpleDateFormat("hh:mm:ss");
    private static SimpleDateFormat dateFormat2 = new SimpleDateFormat("mm:ss");

    static {
        dateFormat1.setTimeZone(TimeZone.getTimeZone("UTC"));
        dateFormat2.setTimeZone(TimeZone.getTimeZone("UTC"));
    }

    public static String getClearWord(String word){
        return word.replaceAll("[^A-Za-z]+", "");
    }

    public static boolean isInteger(String s) {
        try {
            Integer.parseInt(s);
        } catch(NumberFormatException e) {
            return false;
        } catch(NullPointerException e) {
            return false;
        }
        return true;
    }

    @Nullable
    public static Date parseDate(String s, SimpleDateFormat format) {
        try {
            return format.parse(s);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static Date parseDate(String s) {
        Date date = parseDate(s, dateFormat1);
        if (date != null)
            return date;
        return parseDate(s, dateFormat2);
    }

    @NonNull
    public static String clearSentence(String line) {
        if (line == null)
            return "";
        return line.replace("<br/>", " ").replaceAll("<[^>]*>","");
    }
}
