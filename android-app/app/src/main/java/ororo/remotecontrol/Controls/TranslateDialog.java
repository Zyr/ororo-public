package ororo.remotecontrol.Controls;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.widget.LinearLayout;
import android.widget.TextView;

import ororo.remotecontrol.Api.Leo.Translation;
import ororo.remotecontrol.R;
import ororo.remotecontrol.Services.SubtitleService;
import ororo.remotecontrol.SubtitleLine;

public class TranslateDialog {
    private Dialog mDialog;
    private Context context;
    private TextView mTranscription;
    private TextView mWord;
    private TextView mSentence;
    private TextView mTranslatedSentence;
    private LinearLayout mTranslations;
    private SubtitleService mSubtitleService;

    public TranslateDialog(Context context, SubtitleService subtitleService) {
        this.context = context;
        mSubtitleService = subtitleService;
        mDialog = new Dialog(context);
        mDialog.setContentView(R.layout.dialog);
        mTranscription = mDialog.findViewById(R.id.translate_dialog_transcription);
        mWord = mDialog.findViewById(R.id.translate_dialog_word);
        mSentence = mDialog.findViewById(R.id.translate_dialog_sentence);
        mTranslatedSentence = mDialog.findViewById(R.id.translate_dialog_translated_sentence);
        mTranslations = mDialog.findViewById(R.id.translate_dialog_translates);
    }

    public void show() {
        mDialog.show();
    }

    public void setOnDismissListener(DialogInterface.OnDismissListener onDismissListener) {
        mDialog.setOnDismissListener(onDismissListener);
    }

    public void updateTranslation(SubtitleLine subtitleLine, String word, Translation translation) {
        mWord.setText(word);
        mSentence.setText(subtitleLine.Line);
        mTranslatedSentence.setText(mSubtitleService.getContextualTranslate(subtitleLine.From));
        mTranscription.setText("[" + translation.Transcription + "]");
        mTranslations.removeAllViews();
        for (int i = 0; i < translation.Translates.length; i++) {
            TranslateView translateView = new TranslateView(context, i + 1, translation.Translates[i]);
            mTranslations.addView(translateView);
        }
    }
}