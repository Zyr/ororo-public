package ororo.remotecontrol.Controls;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.text.SpannableString;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.method.MovementMethod;
import android.text.style.ClickableSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import ororo.remotecontrol.R;
import ororo.remotecontrol.SubtitleLine;

public class SubtitleLineView extends LinearLayout {

    LinearLayout mSubtitleContainer;

    public SubtitleLineView(Context context, SubtitleLine subtitleLine, OnWordClickListener onClickListener) {
        super(context);
        initComponent(context, subtitleLine, onClickListener);
    }

    @Override
    public void setBackgroundColor(int color) {
        mSubtitleContainer.setBackgroundColor(color);
    }

    private void initComponent(Context context, final SubtitleLine subtitleLine, final OnWordClickListener onClickListener) {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.subtitle_line_view, this);
        mSubtitleContainer = this.findViewById(R.id.subtitle_container);

        TextView textView = this.findViewById(R.id.subtitle_text);
        for (final String word : subtitleLine.Words) {
            SpannableString link = makeLinkSpan(word + " ", new OnClickListener() {
                @Override
                public void onClick(View v) {
                onClickListener.onWordClick(word);
                }
            });
            textView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    onClickListener.onSentenceLongClick(subtitleLine);
                //Protection from null pointer when long tap on text
                return true;
                }
            });
            textView.append(link);
        }
        makeLinksFocusable(textView);
    }

    private SpannableString makeLinkSpan(CharSequence text, View.OnClickListener listener) {
        SpannableString link = new SpannableString(text);
        link.setSpan(new ClickableString(listener), 0, text.length(), SpannableString.SPAN_INCLUSIVE_EXCLUSIVE);
        return link;
    }

    private class ClickableString extends ClickableSpan {
        private View.OnClickListener mListener;

        public ClickableString(View.OnClickListener listener) {
            mListener = listener;
        }

        @Override
        public void onClick(View v) {
            mListener.onClick(v);
        }

        @Override
        public void updateDrawState(TextPaint ds) {
            super.updateDrawState(ds);
            ds.setUnderlineText(false);
            ds.setColor(Color.parseColor("#2E353D"));
        }
    }

    private void makeLinksFocusable(TextView tv) {
        MovementMethod m = tv.getMovementMethod();
        if ((m == null) || !(m instanceof LinkMovementMethod)) {
            if (tv.getLinksClickable()) {
                MovementMethod instance = LinkMovementMethod.getInstance();
                if (instance != null)
                    tv.setMovementMethod(instance);
            }
        }
    }
}