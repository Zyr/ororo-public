package ororo.remotecontrol.Controls;

import android.content.Context;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;

import ororo.remotecontrol.Api.Leo.Translate;
import ororo.remotecontrol.R;

public class TranslateView extends LinearLayout {

    public TranslateView(Context context, int index, Translate translate) {
        super(context);
        initComponent(index, translate);
    }

    private void initComponent(int index, Translate translate) {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.translation, this);
        TextView textView = findViewById(R.id.translate_dialog_translation_text);
        textView.setText(index + ". " + translate.Value);
    }
}
