function signInWithPopup() {
    var provider = new firebase.auth.GoogleAuthProvider();
    provider.addScope('https://www.googleapis.com/auth/contacts.readonly');
    return firebase.auth().signInWithPopup(provider);
}

function signInWithEmailAndPassword(email, password) {
    console.log(email);
    console.log(password);
    return firebase.auth().signInWithEmailAndPassword(email, password);
}

function signIn(signInMethod) {
    document.getElementById('sign-in-by-google').disabled = true;
    document.getElementById('sign-in-by-email').disabled = true;
    if (!firebase.auth().currentUser) {
        signInMethod().then(function(result) {
        }).catch(function(error) {
            updateInterface(null);
            var errorCode = error.code;
            if (errorCode === 'auth/account-exists-with-different-credential') {
                alert('You have already signed up with a different auth provider for that email.');
            } else {
                if (error.message)
                    alert(error.message);
                console.error(error);
            }
        });
    }
}

function signOut() {
    document.getElementById('sign-out').disabled = true;
    firebase.auth().signOut();
}

function updateInterface(user) {
    if (user) {
        document.getElementById('sign-in-container').style.display = 'none';
        document.getElementById('sign-out-container').style.display = 'block';
        document.getElementById('sign-in-by-google').disabled = true;
        document.getElementById('sign-in-by-email').disabled = true;
        document.getElementById('sign-out').disabled = false;
    } else {
        document.getElementById('sign-in-container').style.display = 'block';
        document.getElementById('sign-out-container').style.display = 'none';
        document.getElementById('sign-in-by-google').disabled = false;
        document.getElementById('sign-in-by-email').disabled = false;
        document.getElementById('sign-out').disabled = true;
    }
}
window.addEventListener('load', function() {
    firebase.auth().onAuthStateChanged(function(user) {
        updateInterface(user)
    });

    document.getElementById('sign-in-by-google').addEventListener('click', function() {
        signIn(signInWithPopup);
    }, false);

    document.getElementById('sign-in-by-email').addEventListener('click', function() {
        var email = document.getElementById('sign-in-email').value;
        var password = document.getElementById('sign-in-pass').value;
        signIn(function () {
            return signInWithEmailAndPassword(email, password);
        });
    }, false);

    document.getElementById('sign-out').addEventListener('click', function() {
        signOut();
    }, false);
});