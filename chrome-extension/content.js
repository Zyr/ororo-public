function startObserver(selector, getAttributeValue, callback) {
    var MutationObserver = window.MutationObserver || window.WebKitMutationObserver || window.MozMutationObserver;
    if (!MutationObserver) {
        return;
    }

    var elements = document.querySelectorAll(selector);
    if (elements.length == 0) {
        return null;
    }

    var observer = new MutationObserver(function (mutations) {
        mutations.forEach(function (mutation) {
            if (mutation.type === "attributes") {
                var value = getAttributeValue(elements);
                callback(value);
            }
        });
    });
    observer.observe(elements[0], {attributes: true});
    return observer;
}
function sendExtensionRequest(eventType, value) {
    chrome.extension.sendRequest({
        type: eventType,
        value: value
    });
}

function getCurrentTime(elements) {
    return elements[0].getAttribute('data-current-time');
}

function getSubtitles(elements) {
    var subtitles = [];
    elements.forEach(function(element){
        var name = element.getAttribute('label');
        var url = element.getAttribute('src');
        subtitles.push({
            subtitleName : name,
            subtitleUrl : url
        });
    });
    return subtitles;
}

function sendCurrentSubtitles() {
    var elements = document.querySelectorAll('track[kind=subtitles]');
    if (elements.length == 0)
        return [];
    var subtitles = getSubtitles(elements);
    sendExtensionRequest('SubtitlesChanged', subtitles);
    return subtitles;
}

(function(){
    var url = window.location.href;
    var currentTimeObserver = null;
    var currentSubtitles = null;
    setInterval(function() {
        if (window.location.href != url) {
            url = window.location.href;
            if (currentTimeObserver)
                currentTimeObserver.disconnect();
            currentTimeObserver = null;
            currentSubtitles = null;
        }
        if (!currentSubtitles)
            currentSubtitles = sendCurrentSubtitles();
        if (!currentTimeObserver)
            currentTimeObserver = startObserver('.vjs-play-progress', getCurrentTime, function(value){ sendExtensionRequest('CurrentTimeChanged', value)});
    }, 1000);
})();