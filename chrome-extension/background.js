function initApp() {
    firebase.auth().onAuthStateChanged(function (user) {
        if (user) {
            var userId = user.uid;
            var database = firebase.database();
            var playerStatus = database.ref(userId + '/playerStatus');
            var currentTimeRef = database.ref(userId + '/currentVideo/time');
            var currentSetTimeRef = database.ref(userId + '/currentVideo/setTime');
            var subtitlesRef = database.ref(userId + '/currentVideo/subtitles');

            var previousTime = null;
            chrome.extension.onRequest.addListener(function (request) {
                if (request.type === "CurrentTimeChanged" && previousTime != request.value) {
                    currentTimeRef.set(request.value);
                    previousTime = request.value;
                }
                if (request.type === "SubtitlesChanged") {
                    subtitlesRef.set(request.value);
                    console.log(request.value);
                }
            });

            var first = true;
            playerStatus.on('value', function () {
                if (!first) {
                    var script = 'var button = document.querySelector(".vjs-button"); button && button.click()';
                    chrome.tabs.executeScript({code: script});
                }
                first = false;
            })
			
			var firstCurrentSetTime = true;
			currentSetTimeRef.on('value', function (snapshot) {
                if (!firstCurrentSetTime) {
					console.log(snapshot);
					console.log(snapshot.val());
					var unixTime = snapshot.val();
					var totalSeconds = unixTime / 1000;
					console.log(totalSeconds);
					if (totalSeconds) {
						var script = 'var video = document.querySelector("video"); if (video) video.currentTime = ' + totalSeconds + ";";
						chrome.tabs.executeScript({code: script});
					}
                }
                firstCurrentSetTime = false;
            })
        }
    });
}

window.addEventListener('load', function() {
    initApp();
});